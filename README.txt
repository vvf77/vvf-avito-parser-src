
Usage:

var AvitoParser = require('vvf-avito-parser');
var parser = new AvitoParser( {
    // numberParalleleDownloads: // default 5,
    // numberParalleleImageParser: // default == numberParalleleDownloads,
    // startUrls: (Array) default: [ 'http://www.avito.ru/rossiya/nedvizhimost' ]
    // maxTres: default == 10

    // callbacks / events:
    // Вызов после парсинга одного объявления и загрузки всех его картинок с телефонами:
    // onParseOne / parseOne =function( error, url, html, phone_number )
    // onDone / done
    // - / error - при ошибке
    // - / lastPage - если была распарсена последняя страница в списке
    // - / restart - при ошибке загрузке - перед повтороной попыткой (повторная попытка через некоторое время)
} );
parser.run();